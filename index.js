document.addEventListener("DOMContentLoaded", render);

document.body.style = `
  display: flex; 
  flex-wrap: wrap;
  gap: 20px; 
  align-items: center;
  justify-content: center;
  padding-top: 40px;
`;
//   Функція рендеру, яка передає у функцію createDivs потрібну кількість дивів та додає обробники подій

function render() {
   let divs = createDivs(100);

   divs.forEach((div, i) => {
      let isOnInitialPosition = true;
      div.addEventListener("click", function () {
         if (isOnInitialPosition) {
            document.body.prepend(div);
         } else {
            const nextDiv = divs[i + 1];
            if (nextDiv) {
               document.body.insertBefore(div, nextDiv);
            }
         }
         isOnInitialPosition = !isOnInitialPosition;

         divs = Array.from(document.querySelectorAll('div'));
      });
   });
}


// Функция створення потрібної кількості дивів через цикл

function createDivs(count) {
   const divs = [];
   for (let i = 0; i < count; i++) {
      const div = createDiv(i);
      divs.push(div);
      document.body.append(div);
   }
   return divs;
}


// Функция створення окремого Діву із налаштуваннями
function createDiv(i) {
   const div = document.createElement('div');
   const bgColor = getRandomColor();

   div.style = `
    width: 100px;
    height: 100px;
    font-size: 22px;
    font-weight: 500;
    background-color: ${bgColor};
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
  `;

   div.textContent = `Div: ${i + 1}`;

   return div;
}
// Функція яка повертає рандомний колір у форматі HEX
function getRandomColor() {
   const randomColor = Math.floor(Math.random() * 1000000).toString(16);
   const hexColor = '0'.repeat(6 - randomColor.length) + randomColor;
   return '#' + hexColor;
}


